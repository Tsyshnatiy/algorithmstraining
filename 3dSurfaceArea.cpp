// https://www.hackerrank.com/challenges/3d-surface-area/problem

using Board = vector<vector<std::size_t>>;

struct Cell final
{
    Cell(std::size_t rr, std::size_t cc, std::size_t kk)
        : r{rr},
        c{cc},
        k{kk}
    {}
    
    std::size_t r = 0;
    std::size_t c = 0;
    std::size_t k = 0;    
};

bool isInsideOfToy(const Cell& cell, 
                std::size_t H, 
                std::size_t W,
                const Board& board)
{
    if (cell.r >= H)
    {
        return false;
    }
    
    if (cell.c >= W)
    {
        return false;
    }
    
    return cell.k < board[cell.r][cell.c];
}

int countVoxel(Cell cell, 
                const Board& board,
                std::size_t H, std::size_t W)
{
    int result = 0;
    const std::array<Cell, 6> neighbours
    {
        Cell{cell.r - 1, cell.c, cell.k},
        Cell{cell.r + 1, cell.c, cell.k},
        Cell{cell.r, cell.c - 1, cell.k},
        Cell{cell.r, cell.c + 1, cell.k},
        Cell{cell.r, cell.c, cell.k - 1},
        Cell{cell.r, cell.c, cell.k + 1}
    };
    for (const auto& n : neighbours)
    {
        if (!isInsideOfToy(n, H, W, board))
        {
            ++result;
        }
    }
    
    return result;
}

int surfaceArea(Board A) 
{
    const auto H = A.size();
    
    int result = 0;
    for (std::size_t row = 0 ; row < H ; ++row)
    {
        const auto W = A.front().size();
        for (std::size_t col = 0 ; col < W ; ++col)
        {
            const auto Z = A[row][col];
            for (std::size_t k = 0 ; k < Z ; ++k)
            {
                Cell cell{row, col, k};
                result += countVoxel(cell, A, H, W);
            }
        }
    }
    
    return result;
}