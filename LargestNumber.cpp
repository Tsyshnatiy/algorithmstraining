class Solution {
public:
    std::string largestNumber(const std::vector<int>& nums)
    {
        std::vector<std::string> numbers;
        numbers.reserve(nums.size());
        for (const auto n : nums)
        {
            numbers.push_back(std::to_string(n));
        }

        std::sort(numbers.begin(), numbers.end(),
            [this](const std::string& lhs, const std::string& rhs)
            {
                const auto lhsRhs = lhs + rhs;
                const auto rhsLhs = rhs + lhs;

                return lhsRhs < rhsLhs;
            });

        std::ostringstream oss;
        for (auto it = numbers.rbegin() ; it != numbers.rend() ; ++it)
        {
            oss << *it;
        }

        auto result = oss.str();
        auto it = result.begin();
        while (*it == '0')
        {
            ++it;
        }

        if (it == result.end())
        {
            return "0";
        }

        return result;
    }
};