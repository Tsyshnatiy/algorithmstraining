// https://leetcode.com/problems/lru-cache/

class LRUCache {
public:
    using Key = int;
    using Value = int;
    using Time = std::size_t;
    
    LRUCache(int capacity)
        : m_capacity(capacity)
    {
        
    }
    
    int get(int key)
    {
        auto it = m_cache.find(key);
        if (it == m_cache.cend())
            return -1;
     
        m_currentTime++;
        
        auto value = it->second;
        
        update(key);
        
        return value;
    }
    
    void put(int key, int value)
    {
        if (m_cache.size() == m_capacity && m_cache.find(key) == m_cache.cend())
            evict();
        
        m_currentTime++;
        
        update(key);

        m_cache[key] = value;
    }
    
private:
    template<typename It, typename Collection>
    void assertNotEnd(It it, const Collection& collection)
    {
        if (it == collection.end())
            throw std::runtime_error("No such it");
    }
    
    void update(int key)
    {
        auto keyVsTimeIt = m_keyVsTime.find(key);
        if (keyVsTimeIt != m_keyVsTime.cend())
        {
            const auto oldTime = keyVsTimeIt->second;
            auto timeVsKeyIt = m_timeVsKey.find(oldTime);
            if (timeVsKeyIt != m_timeVsKey.cend())
                m_timeVsKey.erase(timeVsKeyIt);
        }
        
        m_timeVsKey[m_currentTime] = key;
        m_keyVsTime[key] = m_currentTime;
    }
    
    void evict()
    {   
        auto leastTimeElementIt = m_timeVsKey.begin();
        assertNotEnd(leastTimeElementIt, m_timeVsKey);
        
        auto& leastTimeKey = leastTimeElementIt->second;

        auto leastTimeKeyIt = m_keyVsTime.find(leastTimeKey);
        assertNotEnd(leastTimeKeyIt, m_keyVsTime);

        auto leastTimeKeyVsValueIt = m_cache.find(leastTimeKey);
        assertNotEnd(leastTimeKeyVsValueIt, m_cache);
        
        m_timeVsKey.erase(leastTimeElementIt);
        m_keyVsTime.erase(leastTimeKeyIt);
        m_cache.erase(leastTimeKeyVsValueIt);
    }
    
    int m_capacity = 0;
    
    Time m_currentTime = 1;
    
    std::unordered_map<Key, Value> m_cache;  
    std::unordered_map<Key, Time> m_keyVsTime;
    std::map<Time, Key> m_timeVsKey;
};