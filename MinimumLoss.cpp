// https://www.hackerrank.com/challenges/minimum-loss/problem

int minimumLoss(vector<long> price) 
{
    std::set<long, std::greater<long>> tail;
    tail.insert(price.back());
    
    int minLoss = std::numeric_limits<int>::max();
    for (auto rit = price.rbegin() + 1; rit != price.rend() ; ++rit)
    {
        const auto localMaxIt = tail.lower_bound(*rit);
        if (localMaxIt == tail.cend())
        {
            tail.insert(*rit);
            continue;
        }
        const auto loss = *rit - *localMaxIt;
        if (minLoss > loss)
        {
            minLoss = loss;
        }
        
        tail.insert(*rit);
    }
    return minLoss;
}