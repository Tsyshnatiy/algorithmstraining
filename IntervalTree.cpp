// https://leetcode.com/problems/range-sum-query-mutable/

class IntervalTree
{
  public:
    IntervalTree(const vector<int>& nums)
      : m_nums(nums),
      m_tree(nums.size() > 0 ? 4 * nums.size() : 1, TreeNode{})
      {
        if (m_nums.empty())
          return;
        
        build(nums, 0, nums.size() - 1, 0);
      }
  
    void print() const
    {
      for (const auto& node : m_tree)
        cout << node.l << ".." << node.r << " " << node.val << endl;
      cout << endl;
    }
  
    int sum(size_t l, size_t r)
    {
      return doSum(l, r, 0);
    }
  
    void update(size_t idx, int val)
    {
      doUpdate(idx, val, 0);
    }
  
  private:
    struct TreeNode
    {
      size_t l = 0;
      size_t r = 0;
      int val = 0;
    };
  
    int doSum(size_t l, size_t r, size_t nodeIdx)
    {
      if (m_tree[nodeIdx].l > r || m_tree[nodeIdx].r < l)
        return 0;
      
      if (m_tree[nodeIdx].l >= l && m_tree[nodeIdx].r <= r)
        return m_tree[nodeIdx].val;
      
      const auto sl = doSum(l, r, 2 * nodeIdx + 1);
      const auto sr = doSum(l, r, 2 * nodeIdx + 2);
      
      return sl + sr;
    }
  
    void doUpdate(size_t idx, int val, size_t nodeIdx)
    {
      if (m_tree[nodeIdx].l > idx || m_tree[nodeIdx].r < idx)
        return;
      
      else if (m_tree[nodeIdx].l == m_tree[nodeIdx].r)
      {
        m_tree[nodeIdx].val = val;
        return;
      }
      
      doUpdate(idx, val, 2 * nodeIdx + 1);
      doUpdate(idx, val, 2 * nodeIdx + 2);
      
      m_tree[nodeIdx].val = m_tree[2 * nodeIdx + 1].val + m_tree[2 * nodeIdx + 2].val;
    }
  
    void build(const vector<int>& nums, size_t l, size_t r, size_t nodeIdx)
    {
      m_tree[nodeIdx].l = l;
      m_tree[nodeIdx].r = r;
      
      if (l == r)
      {
        m_tree[nodeIdx].val = nums[l];
        return;
      }
      
      const auto mid = (l + r) / 2;
      
      build(nums, l, mid, 2 * nodeIdx + 1);
      build(nums, mid + 1, r, 2 * nodeIdx + 2);
      
      m_tree[nodeIdx].val = m_tree[2 * nodeIdx + 1].val + m_tree[2 * nodeIdx + 2].val;
    }
  
    const vector<int>& m_nums;
    std::vector<TreeNode> m_tree;
};

class NumArray
{
public:
  NumArray(vector<int>& nums)
    : m_tree(nums)
  {
    //m_tree.print();
  }

  void update(int i, int val)
  {
    m_tree.update(i, val);
  }

  int sumRange(int i, int j)
  {
    return m_tree.sum(i, j);
  }
  
private:
  IntervalTree m_tree;
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * obj->update(i,val);
 * int param_2 = obj->sumRange(i,j);
 */