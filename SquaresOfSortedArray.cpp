// https://leetcode.com/problems/squares-of-a-sorted-array/

class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) 
    {
        if (nums.empty())
        {
            return nums;
        }
        
        int max = std::max(std::abs(nums.front()), std::abs(nums.back()));
        
        std::vector<int> counts(max + 1, 0);
        for (const auto e : nums)
        {
            ++counts[std::abs(e)];
        }
        
        const auto numsSize = nums.size();
        nums.clear();
        std::vector<int> result;
        result.reserve(numsSize);
        
        for (std::size_t i = 0 ; i < counts.size() ; ++i)
        {
            const auto squared = i * i;
            for (std::size_t j = 0 ; j < counts[i] ; ++j)
            {
                result.push_back(squared);       
            }
        }
        
        return result;
    }
};