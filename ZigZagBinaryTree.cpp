// https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/

#include <queue>
#include <vector>
#include <utility>
#include <iostream>

struct TreeNode
{
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    using Level = std::vector<int>;
    using Result = std::vector<Level>;
    using LevelQueue = std::queue<std::pair<TreeNode*, int>>;

    std::vector<TreeNode*> getNextLevelNodes(LevelQueue& q, int level)
    {
        std::vector<TreeNode*> result;
        while (!q.empty())
        {
            auto [n, l] = q.front();
            if (l != level)
            {
                break;
            }

            q.pop();
            result.push_back(n);
        }

        return result;
    }

    Result zigzagLevelOrder(TreeNode* root)
    {
        Result result;
        if (!root)
        {
            return result;
        }

        std::queue<std::pair<TreeNode*, int>> nodes;
        nodes.push(std::make_pair(root, 0));

        int currentLevel = 0;
        auto levelNodes = getNextLevelNodes(nodes, currentLevel);
        while (!levelNodes.empty())
        {
            result.emplace_back();
            for (auto* n : levelNodes)
            {
                if (n->left)
                {
                    nodes.push(std::make_pair(n->left, currentLevel + 1));
                }
                if (n->right)
                {
                    nodes.push(std::make_pair(n->right, currentLevel + 1));
                }
            }

            if (currentLevel % 2 == 0)
            {
                for (auto it = levelNodes.begin();
                    it != levelNodes.end();
                    ++it)
                {
                    result.back().push_back((*it)->val);
                }
            }
            else
            {
                for (auto it = levelNodes.rbegin();
                    it != levelNodes.rend();
                    ++it)
                {
                    result.back().push_back((*it)->val);
                }
            }

            ++currentLevel;
            levelNodes = getNextLevelNodes(nodes, currentLevel);
        }

        return result;
    }
};

int main()
{
    TreeNode* root = new TreeNode{3};
    root->left = new TreeNode{ 2 };
    root->right = new TreeNode{ 1 };
    root->right = new TreeNode{ 4 };
    root->right->left = new TreeNode{ 6 };
    root->right->right = new TreeNode{ 8 };

    auto t = Solution().zigzagLevelOrder(root);
    for (auto& tt : t)
    {
        for (auto& ttt : tt)
        {
            std::cout << ttt << " ";
        }

        std::cout << std::endl;
    }

    // Delete allocated tree
    std::queue<TreeNode*> nodes;
    nodes.push(root);
    while (!nodes.empty())
    {
        auto* n = nodes.front();
        nodes.pop();
        if (n->left)
        {
            nodes.push(n->left);
        }
        if (n->right)
        {
            nodes.push(n->right);
        }
        delete n;
    }
    return 0;
}
