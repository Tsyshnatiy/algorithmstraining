/*
Finds editing distance between words
https://en.wikipedia.org/wiki/Edit_distance

Sample Input:
short
ports

Sample Output:
3
*/

#include <iostream>
#include <vector>
#include <algorithm>

int getResult(const std::string& a, const std::string& b)
{
  int n = a.size() + 1;
  int m = b.size() + 1;

  std::vector<std::vector<int> > D;
  D.reserve(n);

  for (size_t i = 0 ; i < n ; ++i)
  {
    std::vector<int> di(m, std::numeric_limits<int>::max());
    D.push_back(std::move(di));
  }

  const auto diff = [&a, &b] (size_t i, size_t j)
  {
    return a[i - 1] == b[j - 1] ? 0 : 1;
  };

  for (size_t i = 0 ; i < n; ++i)
    D[i][0] = i;
  for (size_t i = 0 ; i < m; ++i)
    D[0][i] = i;

  for (size_t i = 1 ; i < n ; ++i)
  {
    for (size_t j = 1 ; j < m ; ++j)
    {
      auto ins = D[i - 1][j] + 1;
      auto del = D[i][j - 1] + 1;
      auto rep = D[i - 1][j - 1] + diff(i, j);

      D[i][j] = std::min({ ins, del, rep });
    }
  }

  return D[n - 1][m - 1];
}

int main(void)
{
  std::string a;
  std::string b;

  std::cin >> a;
  std::cin >> b;

  const auto res = getResult(a, b);
  std::cout << res << std::endl;
}