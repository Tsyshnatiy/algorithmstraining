// https://www.hackerrank.com/challenges/beautiful-pairs/problem

int beautifulPairs(vector<int> A, vector<int> B) {
    int result = 0;
    
    std::unordered_map<int, int> aCount;
    std::unordered_map<int, int> bCount;
    
    for (std::size_t i = 0 ; i < A.size() ; ++i)
    {
        ++aCount[A[i]];
        ++bCount[B[i]];
    }
    
    bool foundMismatch = false;
    for (const auto& entry : aCount)
    {
        const auto val = entry.first;
        const auto aCnt = entry.second;
        
        const auto it = bCount.find(val);
        if (it == bCount.cend())
        {
            result += foundMismatch ? 0 : 1;
            foundMismatch = true;
            continue;
        }
        
        const auto bCnt = it->second;
        if (aCnt == bCnt)
        {
            result += aCnt;
        }
        else
        {
            result += std::min(aCnt, bCnt);
            result += foundMismatch ? 0 : 1;
            foundMismatch = true;
        }
    }
    
    if (!foundMismatch)
    {
        --result;
    }
    
    return result;
}