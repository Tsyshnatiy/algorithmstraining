/*
Checks if brackets are positioned correctly
If not then output error position
*/

#include <algorithm>
#include <cstddef>
#include <iostream>
#include <vector>
#include <iterator>
#include <stack>
#include <map>

template<typename T>
void print(const std::vector<T>& numbers)
{
  std::copy(numbers.cbegin(), numbers.cend(),std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}

int checkString(const std::string& str)
{
  std::stack<std::pair<int, size_t> > stack;
  std::map<char, int> charMap
    {
      { '(', 1 },
      { '[', 2 },
      { '{', 3 },
      { '}', -3 },
      { ']', -2 },
      { ')', -1 },
    };

  for (size_t i = 0 ; i < str.size() ; ++i)
  {
    auto it = charMap.find(str[i]);
    if (it == charMap.cend())
      continue;

    if (it->second > 0)
      stack.push({ it->second, i });
    else
    {
      if (stack.empty())
        return static_cast<int>(i) + 1;

      auto lastVal = stack.top();
      stack.pop();
      if (lastVal.first != -1 * it->second)
        return static_cast<int>(i) + 1;
    }
  }

  if (!stack.empty())
    return static_cast<int>(stack.top().second) + 1;

  return -1;
}

int main(void)
{
  std::string input;// = "()({}";

  std::cin >> input;

  auto result = checkString(input);

  if (result < 0)
    std::cout << "Success" << std::endl;
  else
    std::cout << result << std::endl;
}