/*
Sample Input:

6
Insert 200
Insert 10
ExtractMax
Insert 5
Insert 500
ExtractMax
Sample Output:

200
500
*/

#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>

class BinaryHeap
{
public:
    void insert(int value)
    {
      m_data.push_back(value);
      siftUp();
    }

    int extractMax()
    {
      if (m_data.empty())
        return 0;

      int result = m_data.front();

      std::swap(m_data[0], m_data[m_data.size() - 1]);
      m_data.pop_back();

      siftDown();

      return result;
    }

private:
    void siftUp()
    {
      size_t curIdx = m_data.size() - 1;
      size_t pIdx = parentIdx(curIdx);

      while(m_data[curIdx] > m_data[pIdx])
      {
        std::swap(m_data[curIdx], m_data[pIdx]);
        curIdx = pIdx;
        pIdx = parentIdx(curIdx);
      }
    }

    void siftDown()
    {
      size_t curIdx = 0;

      while(true)
      {
        size_t lChild = leftChild(curIdx);
        size_t rChild = rightChild(curIdx);

        size_t swapIdx = curIdx;
        if (lChild < m_data.size() && rChild < m_data.size())
        {
          const auto leftValue = m_data[lChild];
          const auto rightValue = m_data[rChild];

          swapIdx = leftValue > rightValue ? lChild : rChild;
        }
        else if (lChild < m_data.size())
          swapIdx = lChild;

        if (m_data[swapIdx] > m_data[curIdx])
        {
          std::swap(m_data[swapIdx], m_data[curIdx]);
          curIdx = swapIdx;
        }
        else
          break;
      }
    }

    size_t parentIdx(size_t child) const
    {
      if (child == 0)
        return 0;

      return static_cast<size_t>(std::ceil(child * 1.0f / 2)) - 1;
    }

    size_t leftChild(size_t p) const
    {
      return p * 2 + 1;
    }

    size_t rightChild(size_t p) const
    {
      return p * 2 + 2;
    }

    std::vector<int> m_data;
};

int main(void)
{
  BinaryHeap heap;

  int n = 0;
  std::cin >> n;

  for (int i = 0 ; i < n ; ++i)
  {
    std::string command;
    std::cin >> command;

    std::transform(command.begin(), command.end(), command.begin(), ::tolower);

    if (command == "insert")
    {
      int value;
      std::cin >> value;

      heap.insert(value);
    }
    else if (command == "extractmax")
    {
      std::cout << heap.extractMax() << std::endl;
    }
  }
}