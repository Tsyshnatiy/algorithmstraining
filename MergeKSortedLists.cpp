/**
Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.

Example:

Input:
[
  1->4->5,
  1->3->4,
  2->6
]
Output: 1->1->2->3->4->4->5->6
**/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution
{
public:
  ListNode* mergeKLists(vector<ListNode*>& lists)
  {
    ListNode* pResult = nullptr;
    ListNode* pCurrent = pResult;
    
    multiset<ListNode*, Comparer> cursors;
    for (auto pList : lists)
      if (pList)
        cursors.insert(pList);
    
    while(!cursors.empty())
    {
      if (!pResult)
      {
        pResult = new ListNode(0);
        pCurrent = pResult;
      }
      else
      {
        pCurrent->next = new ListNode(0);
        pCurrent = pCurrent->next;
      }
      
      auto it = cursors.begin();
      auto minVal = (*it)->val;
      pCurrent->val = minVal;
      
      auto pCursor = (*it);
      pCursor = pCursor->next;
      
      cursors.erase(it);
      
      if (pCursor)
        cursors.insert(pCursor);
    }
    
    return pResult;
  }
  
private:
  struct Comparer
  {
    bool operator () (const ListNode* lhs, const ListNode* rhs) const
    {
      return lhs->val < rhs->val;
    }
  };
};